# xJaMa

## Extended Version of the original Jama-Class from MathWorks

 Jama = Java Matrix class.
 
 The Java Matrix Class provides the fundamental operations of numerical linear
 algebra. Various constructors create Matrices from two dimensional arrays of
 double precision floating point numbers. Various "gets" and "sets" provide
 access to submatrices and matrix elements. Several methods implement basic
 matrix arithmetic, including matrix addition and multiplication, matrix
 norms, and element-by-element array operations. Methods for reading and
 printing matrices are also included. All the operations in this version of
 the Matrix Class involve real matrices. Complex matrices may be handled in a
 future version.
  
 Five fundamental matrix decompositions, which consist of pairs or triples of
 matrices, permutation vectors, and the like, produce results in five
 decomposition classes. These decompositions are accessed by the Matrix class
 to compute solutions of simultaneous linear equations, determinants, inverses
 and other matrix functions. The five decompositions are:
 1. Cholesky Decomposition of symmetric, positive definite matrices.
 2. Decomposition of rectangular matrices.
 3. QR Decomposition of rectangular matrices.
 4. Singular Value Decomposition of rectangular matrices.
 5. Eigenvalue Decomposition of both symmetric and nonsymmetric square matrices.
 
 ## Example of use:
 
 Solve a linear system A x = b and compute the residual norm, ||b - A x||.
 
 double[][] vals = {{1.,2.,3},{4.,5.,6.},{7.,8.,10.}};

 Matrix A = new Matrix(vals);

 Matrix b = Matrix.random(3,1);

 Matrix x = A.solve(b);

 Matrix r = A.times(x).minus(b);

 double rnorm = r.normInf();

## Extensions
 xJama provides the FMatrix class for float values and some minor additions to the matrix functions.
 
 @author The MathWorks, Inc. and the National Institute of Standards and Technology. Extended by Ralf Langewiesche.
 
 @version 5 August 1998, Ext 2023-10-29
