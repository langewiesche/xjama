package example;


import xjama.Matrix;

/**
 * Beispielprogramm zur Verwendung der JAMA-Bibliothek fuer die Matrizenrechnung
 *
 * @version 0.5
 * @author rla
 */
public class Main {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {

        // Matrix( double[], zeilen)
        Matrix a = new Matrix(new double[]{1, 2, 3}, 3);
        System.out.print("a = ");
        // print( Anzahl der Ziffern, Anzahl der Nachkommastellen)
        a.print(3, 1);
        System.out.println();
        
        // TODO b und c

        // Matrix( double[][], zeilen, spalten);  // Angabe von zeilen und spalten ist optional
        Matrix A = new Matrix(new double[][]{{1, 2, 3}, {2, 3, 4}});
        System.out.print("A = ");
        A.print(3, 1);
        System.out.println();

        // TODO B und C
        
        Matrix aT = a.transpose();
        System.out.print("1. aT = ");
        aT.print(3, 1);
        System.out.println();
        
        // 2. can't be calculated but try anyway.
        
        
        // TODO 3. - 21.
       
    }

}
