package display;

import xjama.Matrix;
import java.awt.Color;
import java.awt.image.BufferedImage;

/**
 * Wrapper for BufferedImage.
 *
 * @version 2.3
 * @author rla
 */
public class BuImgWrapper extends BufferedImage {

    /**
     * Constructs a BufferedImage wrapper class of one of the predefined image
     * types. The ColorSpace for the image is the default sRGB space.
     *
     * @param width width of the created image
     * @param height height of the created image
     * @param imageType type of the created image
     */
    public BuImgWrapper(int width,
            int height,
            int imageType) {
        super(width, height, imageType);
    }

    /**
     * Constructs a BufferedImage wrapper class from an BufferedImage.
     *
     * @param bi BufferedImage
     */
    public BuImgWrapper(BufferedImage bi) {
        super(bi.getWidth(), bi.getHeight(), bi.getType());
        this.setData(bi.getData());
    }

    /**
     * Returns the color of a pixel. There are only 8-bits of precision for each
     * color component in the returned data when using this method. An
     * ArrayOutOfBoundsException may be thrown if the coordinates are not in
     * bounds. However, explicit bounds checking is not guaranteed.
     *
     * @param x the X coordinate of the pixel from which to get the pixel
     * @param y the Y coordinate of the pixel from which to get the pixel
     * @return the Color object for the pixel
     */
    public Color getColor(int x, int y) {
        return new Color(this.getRGB(x, y));
    }

    /**
     * Returns the color vector of a pixel. There are only 8-bits of precision
     * for each color component in the returned data when using this method. The
     * values for each color component are between 0.0 and 1.0. An
     * ArrayOutOfBoundsException may be thrown if the coordinates are not in
     * bounds. However, explicit bounds checking is not guaranteed.
     *
     * @param x the X coordinate of the pixel from which to get the pixel
     * @param y the Y coordinate of the pixel from which to get the pixel
     * @return the Color vector for the pixel
     */
    public Matrix getColorVector(int x, int y) {
        Color col = getColor(x, y);
        Matrix colV = new Matrix(new double[]{
            col.getRed() / 255.0,
            col.getGreen() / 255.0,
            col.getBlue() / 255.0
        }, 3);
        return colV;
    }

    /**
     * Returns all color vectors of an image as 3 x (width * height) matrix.
     * There are only 8-bits of precision for each color component in the
     * returned data when using this method. The values for each color component
     * are between 0.0 and 1.0.
     *
     * @return the color matrix for the whole image
     */
    public Matrix getColorMatrix() {
        int width = this.getWidth();
        int heigth = this.getHeight();
        Matrix colM = new Matrix(3, width * heigth);
        int i = 0;
        for (int y = 0; y < heigth; ++y) {
            for (int x = 0; x < width; ++x) {
                Color col = getColor(x, y);
                colM.set(0, i, col.getRed() / 255.0);
                colM.set(1, i, col.getGreen() / 255.0);
                colM.set(2, i, col.getBlue() / 255.0);
                ++i;
            }
        }
        return colM;
    }

    /**
     * Returns all red values of an image as matrix(height, width). There are
     * only 8-bits of precision for each color component in the returned data
     * when using this method. The values for each color component are between
     * 0.0 and 1.0. An ArrayOutOfBoundsException may be thrown if the
     * coordinates are not in bounds. However, explicit bounds checking is not
     * guaranteed.
     *
     * @return the red matrix for the whole image
     */
    public Matrix getRedMatrix() {
        int width = this.getWidth();
        int heigth = this.getHeight();
        Matrix colM = new Matrix(heigth, width);
        for (int y = 0; y < heigth; ++y) {
            for (int x = 0; x < width; ++x) {
                Color col = getColor(x, y);
                colM.set(y, x, col.getRed() / 255.0);
            }
        }
        return colM;
    }

    /**
     * Returns all green values of an image as matrix(height, width). There are
     * only 8-bits of precision for each color component in the returned data
     * when using this method. The values for each color component are between
     * 0.0 and 1.0.
     *
     * @return the green matrix for the whole image
     */
    public Matrix getGreenMatrix() {
        int width = this.getWidth();
        int heigth = this.getHeight();
        Matrix colM = new Matrix(heigth, width);
        for (int y = 0; y < heigth; ++y) {
            for (int x = 0; x < width; ++x) {
                Color col = getColor(x, y);
                colM.set(y, x, col.getGreen() / 255.0);
            }
        }
        return colM;
    }

    /**
     * Returns all blue values of an image as matrix(height, width). There are
     * only 8-bits of precision for each color component in the returned data
     * when using this method. The values for each color component are between
     * 0.0 and 1.0. An ArrayOutOfBoundsException may be thrown if the
     * coordinates are not in bounds. However, explicit bounds checking is not
     * guaranteed.
     *
     * @return the blue matrix for the whole image
     */
    public Matrix getBlueMatrix() {
        int width = this.getWidth();
        int heigth = this.getHeight();
        Matrix colM = new Matrix(heigth, width);
        for (int y = 0; y < heigth; ++y) {
            for (int x = 0; x < width; ++x) {
                Color col = getColor(x, y);
                colM.set(y, x, col.getBlue() / 255.0);
            }
        }
        return colM;
    }

    /**
     * Sets a pixel in this image to the specified color. An
     * ArrayOutOfBoundsException may be thrown if the coordinates are not in
     * bounds. However, explicit bounds checking is not guaranteed.
     *
     * @param x the X coordinate of the pixel to set
     * @param y the Y coordinate of the pixel to set
     * @param col the new color of the pixel
     */
    public void setColor(int x, int y, Color col) {
        this.setRGB(x, y, col.getRGB());
    }

    /**
     * Sets a pixel in this image to the specified color. The color must be
     * given as Matrix( 3, width * height) with the values ( r, g, b) between
     * 0.0 and 1.0. All color components will be limited between 0.0 and 1.0 in
     * 8 bit precision.
     *
     * @param x the X coordinate of the pixel to set
     * @param y the Y coordinate of the pixel to set
     * @param f the new red part of the pixel
     */
    public void setRed(int x, int y, double f) {
        Color col = this.getColor(x, y);
        Color nCol = new Color(rgbLimit(f), col.getGreen() / 255f, col.getBlue() / 255f);
        this.setRGB(x, y, nCol.getRGB());
    }

    /**
     * Sets a pixel in this image to the specified colorplanevalue. The values
     * will be limited to 8 Bit precision.
     *
     * @param x the X coordinate of the pixel to set
     * @param y the Y coordinate of the pixel to set
     * @param f the new Green part of the pixel
     */
    public void setGreen(int x, int y, double f) {
        Color col = this.getColor(x, y);
        Color nCol = new Color(col.getRed() / 255f, rgbLimit(f), col.getBlue() / 255f);
        this.setRGB(x, y, nCol.getRGB());
    }

    /**
     * Sets a pixel in this image to the specified colorplanevalue. The values
     * will be limited to 8 Bit precision.
     *
     * @param x the X coordinate of the pixel to set
     * @param y the Y coordinate of the pixel to set
     * @param f the new red part of the pixel
     */
    public void setBlue(int x, int y, double f) {
        Color col = this.getColor(x, y);
        Color nCol = new Color(col.getRed() / 255f, col.getGreen() / 255f, rgbLimit(f));
        this.setRGB(x, y, nCol.getRGB());
    }

    /**
     * Sets a pixel in this image to the specified color. The color must be
     * given as Matrix( 3, width * height) with the values ( r, g, b) between
     * 0.0 and 1.0. All color components will be limited between 0.0 and 1.0 in
     * 8 bit precision.
     *
     * @param x the X coordinate of the pixel to set
     * @param y the Y coordinate of the pixel to set
     * @param colV the new color of the pixel
     */
    public void setColorVector(int x, int y, Matrix colV) {
        Color col = new Color(
                rgbLimit(colV.get(0, 0)),
                rgbLimit(colV.get(1, 0)),
                rgbLimit(colV.get(2, 0)));
        this.setRGB(x, y, col.getRGB());
    }

    /**
     * Sets all pixels in this image to the specified color. The color must be
     * given as Matrix( 3, 1) with the values ( r, g, b). All color components
     * will be limited between 0.0 and 1.0 in 8 bit precision. An
     * ArrayOutOfBoundsException may be thrown if the coordinates are not in
     * bounds. However, explicit bounds checking is not guaranteed.
     *
     * @param colM the new color of the pixel
     */
    public void setColorMatrix(Matrix colM) {
        int width = this.getWidth();
        int heigth = this.getHeight();
        int i = 0;
        for (int y = 0; y < heigth; ++y) {
            for (int x = 0; x < width; ++x) {
                Color col = new Color(
                        rgbLimit(colM.get(0, i)),
                        rgbLimit(colM.get(1, i)),
                        rgbLimit(colM.get(2, i)));
                this.setRGB(x, y, col.getRGB());
                ++i;
            }
        }
    }

    /**
     * Sets the red plane of all pixels in this image to the specified color.
     * All color components will be limited between 0.0 and 1.0 in 8 bit
     * precision. An ArrayOutOfBoundsException may be thrown if the coordinates
     * are not in bounds. However, explicit bound checking is not guaranteed.
     *
     * @param colM the new red plane of the image
     */
    public void setRedMatrix(Matrix colM) {
        int width = this.getWidth();
        int heigth = this.getHeight();
        for (int y = 0; y < heigth; ++y) {
            for (int x = 0; x < width; ++x) {
                float green = this.getColor(x, y).getGreen();
                float blue = this.getColor(x, y).getBlue();
                this.setRGB(x, y, new Color((float) colM.get(x, y), green, blue).getRGB());
            }
        }
    }

    /**
     * Sets the green plane of all pixels in this image to the specified color.
     * All color components will be limited between 0.0 and 1.0 in 8 bit
     * precision. An ArrayOutOfBoundsException may be thrown if the coordinates
     * are not in bounds. However, explicit bound checking is not guaranteed.
     *
     * @param colM the new green plane of the image
     */
    public void setGreenMatrix(Matrix colM) {
        int width = this.getWidth();
        int heigth = this.getHeight();
        for (int y = 0; y < heigth; ++y) {
            for (int x = 0; x < width; ++x) {
                float red = this.getColor(x, y).getRed();
                float blue = this.getColor(x, y).getBlue();
                this.setRGB(x, y, new Color(red, (float) colM.get(x, y), blue).getRGB());
            }
        }
    }

    /**
     * Sets the blue plane of all pixels in this image to the specified color.
     * All color components will be limited between 0.0 and 1.0 in 8 bit
     * precision. An ArrayOutOfBoundsException may be thrown if the coordinates
     * are not in bounds. However, explicit bound checking is not guaranteed.
     *
     * @param colM the new blue plane of the image
     */
    public void setBlueMatrix(Matrix colM) {
        int width = this.getWidth();
        int heigth = this.getHeight();
        for (int y = 0; y < heigth; ++y) {
            for (int x = 0; x < width; ++x) {
                float red = this.getColor(x, y).getRed();
                float green = this.getColor(x, y).getGreen();
                this.setRGB(x, y, new Color(red, green, (float) colM.get(x, y)).getRGB());
            }
        }
    }

    // Begrenzt Farbkomponenten
    private static int rgbLimit(double in) {
        int rgb = (int) (in * 255 + 0.5);
        if (rgb > 255) {
            return 255;
        } else if (rgb < 0) {
            return 0;
        } else {
            return rgb;
        }
    }

    /**
     * Updates the red plane of all pixels in this image. All color components
     * will be limited between 0.0 and 1.0 in 8 bit precision.
     *
     * @param colM the new red plane of the image
     */
    public void updateRed(Matrix colM) {
        int width = this.getWidth();
        int heigth = this.getHeight();
        for (int y = 0; y < heigth; ++y) {
            for (int x = 0; x < width; ++x) {
                Color alt = this.getColor(x, y);
                Color neu = new Color((int) (colM.get(y, x) * 255.9), alt.getGreen(), alt.getBlue());
                this.setColor(x, y, neu);
            }
        }
    }

    /**
     * Updates the green plane of all pixels in this image. All color components
     * will be limited between 0.0 and 1.0 in 8 bit precision.
     *
     * @param colM the new green plane of the image
     */
    public void updateGreen(Matrix colM) {
        int width = this.getWidth();
        int heigth = this.getHeight();
        for (int y = 0; y < heigth; ++y) {
            for (int x = 0; x < width; ++x) {
                Color alt = this.getColor(x, y);
                Color neu = new Color(alt.getRed(), (int) (colM.get(y, x) * 255.9), alt.getBlue());
                this.setColor(x, y, neu);
            }
        }
    }

    /**
     * Updates the blue plane of all pixels in this image. All color components
     * will be limited between 0.0 and 1.0 in 8 bit precision.
     *
     * @param colM the new blue plane of the image
     */
    public void updateBlue(Matrix colM) {
        int width = this.getWidth();
        int heigth = this.getHeight();
        for (int y = 0; y < heigth; ++y) {
            for (int x = 0; x < width; ++x) {
                Color alt = this.getColor(x, y);
                Color neu = new Color(alt.getRed(), alt.getGreen(), (int) (colM.get(y, x) * 255.9));
                this.setColor(x, y, neu);
            }
        }
    }

}
